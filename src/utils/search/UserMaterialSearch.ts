import {PerceptionType} from "../../enum/PerceptionType";
import {BaseSearch} from "./BaseSearch";

export class UserMaterialSearch extends BaseSearch{
    public perceptions: PerceptionType[] | null = null;
    public constructor(page: number,keyword: string,perceptions: PerceptionType[]) {
        super(page,keyword);
        if(perceptions && perceptions.length > 0){
            this.perceptions = perceptions;
        }
    }
}
