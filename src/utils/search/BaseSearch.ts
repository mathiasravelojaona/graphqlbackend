export abstract class BaseSearch {
    public page: number = 1;
    public keyword: string = '';
    constructor(page: number, keyword: string) {
        if(page){
            this.page = page;
        }
        if(keyword && keyword !== ''){
            this.keyword = keyword;
        }
    }
}
