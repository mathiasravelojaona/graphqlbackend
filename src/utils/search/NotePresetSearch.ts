import {BaseSearch} from "./BaseSearch";
import {AmplitudeModulationType} from "../../enum/AmplitudeModulationType";
import {PerceptionType} from "../../enum/PerceptionType";

export class NotePresetSearch extends BaseSearch{
    perceptionType: PerceptionType;
    constructor(page: number,
                keyword: string,
                perceptionType: PerceptionType) {
        super(page, keyword);
        this.perceptionType = perceptionType;
    }
}
