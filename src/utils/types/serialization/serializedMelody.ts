import { SerializedNote } from './serializedNote';

export interface SerializedMelody {
  m_mute: number;

  m_volume: number;

  m_notes: SerializedNote[];
}
