
import { SerializedSignal } from './serializedSignal';
import {TimeUnit} from "../../../enum/TimeUnit";
import {LengthUnit} from "../../../enum/LengthUnit";

export interface SerializedMaterial {
  m_HDFlag: boolean;

  m_description: string;

  m_length_unit: LengthUnit;

  m_time_unit: TimeUnit;

  m_version: string;

  m_volume: number;

  m_texture: SerializedSignal;

  m_vibration: SerializedSignal;

  m_stiffness: SerializedSignal;
}
