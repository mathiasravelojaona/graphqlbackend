import {SerialisedKeyframe} from "./serialisedKeyframe";
import {PatternFunctions} from "../../../enum/PatternFunctions";

export interface SerializedSignal {
  m_amplitude: number;
  m_frequency: number;
  m_phase: number;
  m_signalType: PatternFunctions;
  m_verticalOffset: number;
  m_keyFrames: SerialisedKeyframe[];
}
