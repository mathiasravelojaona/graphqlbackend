import { SerializedMelody } from './serializedMelody';

export class SerializedPerception {
  m_loop: number;
  m_maximum: number;
  m_melodies: SerializedMelody[];
}
