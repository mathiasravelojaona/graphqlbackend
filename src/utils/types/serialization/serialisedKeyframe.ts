export interface SerialisedKeyframe {
  m_inSlope: number;

  m_inWeight: number;

  m_outSlope: number;

  m_outWeight: number;

  m_tangentMode: number;

  m_time: number;

  m_value: number;

  m_weightedMode: number;
}
