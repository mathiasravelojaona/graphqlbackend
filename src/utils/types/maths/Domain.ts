import {MathExceptions} from "../../../errors/MathExceptions";
import Big from "big.js";

export class Domain {
    inferiorLimit: number;

    superiorLimit: number;

    public constructor(inferiorLimit: number, superiorLimit: number) {
        if (inferiorLimit > superiorLimit) {
            throw new Error(MathExceptions.DOMAIN_EXCEPTION);
        }
        this.inferiorLimit = inferiorLimit;
        this.superiorLimit = superiorLimit;
    }

    getDomainSize(): number {
        return this.superiorLimit - this.inferiorLimit;
    }

    public static generateSubDomains(
        start: number,
        superiorLimit: number,
        subDomainSize: number,
        margin = 0
    ): Domain[] {
        let currentStart: Big = new Big(start);
        let currentEnd: Big = new Big(start);
        const domainList: Domain[] = [];
        const numberOfDomains = Math.ceil((superiorLimit - start) / subDomainSize);
        console.log('8', numberOfDomains);
        for (let i = 0; i < numberOfDomains; i++) {
            currentEnd = currentEnd.add(subDomainSize);
            domainList.push(new Domain(currentStart.toNumber(), currentEnd.toNumber()));
            if (currentStart.add(subDomainSize + margin).gte(superiorLimit)) {
                currentStart = currentStart.add(subDomainSize + margin);
            } else {
                currentStart = currentStart.add(subDomainSize);
            }
        }
        return domainList;
    }
}
