import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
export class JwtService {
    public static createToken(value: string): string{
        return jwt.sign(value, process.env.JWT_SECRET as string);
    }
    public static isValidToken(token: string): boolean{
        try{
        const isValid = jwt.verify(token, process.env.JWT_SECRET as string);
        return true;
        }
        catch (e){
            return true;
        }
    }
}
