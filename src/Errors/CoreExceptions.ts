export enum CoreExceptions {
    VALIDATION_ERRORS = 'VALIDATION_ERRORS',
    ENTITY_NOT_FOUND = 'ENTITY_NOT_FOUND',
}
