export enum PatternFunctions {
    CONSTANT = 0,
    SINUS = 1,
    SQUARE = 2,
    TRIANGLE = 3,
    SAWTOOTH = 4,
}
