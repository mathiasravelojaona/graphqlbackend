export enum EvaluateSteps {
    MATERIAL = .0001,
    STIFFNESS = .0001,
    CARRIER_SIGNAL = .0001,
    RESULT_NOTE = .00001,
    AMPLITUDE_MODULATION= .0001,
}
export enum EvaluateMax {
    STIFFNESS = 1,
    MATERIAL = 5,
}
