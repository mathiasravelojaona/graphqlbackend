import {registerEnumType} from "type-graphql";

export enum GroupSearchEnum {
    ALL,
    MINE,
    SHARED,
}
registerEnumType(GroupSearchEnum, {
   name: 'GroupSearch',
});
