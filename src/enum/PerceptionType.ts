import {registerEnumType} from "type-graphql";


export enum PerceptionType {
    STIFFNESS = 0,
    TEXTURE = 1,
    VIBRATION = 2,
}
registerEnumType(PerceptionType, {
   name: 'PerceptionTypeEnum',
});
