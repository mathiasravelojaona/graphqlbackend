import {registerEnumType} from "type-graphql";

export enum TimeUnit {
    SECOND = 0,
    HOUR = 1,
    MINUTE = 2,
}
registerEnumType(TimeUnit, {
   name: 'TimeUnitEnum',
});
