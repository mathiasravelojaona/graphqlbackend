import {registerEnumType} from "type-graphql";

export enum AmplitudeModulationType {
    PERIODIC_SIGNAL,
    ANIMATION_CURVE,
}
registerEnumType(AmplitudeModulationType, {
   name: 'AmplitudeModulationType',
});
