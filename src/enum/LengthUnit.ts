import {registerEnumType} from "type-graphql";

export enum LengthUnit {
    METER = 0,
    DECIMETER = 1,
    CENTIMETER = 2,
    MILLIMETER = 3,
    INCH = 4,
    FOOT = 5,
    YARD = 6,
}
registerEnumType(LengthUnit, {
    name: 'LengthUnitEnum',
});

