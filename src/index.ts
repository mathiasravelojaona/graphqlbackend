import * as dotenv from 'dotenv';
dotenv.config({
  path: __dirname + '/.env',
});

import "reflect-metadata";
import express from "express";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import cors from 'cors'
import connectDB from "./config/database";
import {Context} from "./Graphql/core/context";
import {Resolvers} from "./Graphql/resolvers";

(async () => {
  try{
  const app = express();

  app.use(
    cors({
      origin: "http://localhost:3000",
      credentials: true
    }),
  )

  await connectDB();

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [...Resolvers],
    }),
    introspection: true,
    playground: true,
    context: ({req,res}): Context<any> => ({
      userId: null,
      request: req,
      materialId: null,
      additionalData: null,
    }),
  });


  apolloServer.applyMiddleware({ app, cors: false });
// Connect to database
  app.listen(process.env.PORT, () => {
    console.log(`express server started at ${process.env.PORT}`);
  });
  }
  catch (e){
    console.log(e);
  }
})();
