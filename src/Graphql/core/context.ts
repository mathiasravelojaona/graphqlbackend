import {Request} from "express"

export interface Context<T extends any>{
    userId: string | null;
    request: Request;
    materialId: number | null;
    additionalData: T;
}
