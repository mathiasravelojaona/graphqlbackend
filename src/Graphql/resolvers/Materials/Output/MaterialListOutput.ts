import BasePaginationModel from "../../../../entity/models/BasePaginationModel";
import {Material} from "../../../../entity/models/Material";
import {ObjectType} from "type-graphql";

@ObjectType()
export class MaterialListOutput extends BasePaginationModel(Material){}
