import {PerceptionWithOutputType} from "../../../../entity/types/PerceptionWithOutputType";
import {Field, ObjectType} from "type-graphql";
import {mongoose, prop} from "@typegoose/typegoose";

@ObjectType()
export class MaterialDetailsOutput {
    @Field({nullable: false})
    _id: string;

    @Field({nullable: false})
    title: string;

    @Field(() => [PerceptionWithOutputType],{nullable: false})
    perceptions: PerceptionWithOutputType[];
    @Field({nullable: false})
    description: string;
    @Field({nullable: false})
    hd: boolean;
    @Field({nullable: false})
    serialisation: string;
    @Field({nullable: true})
    createdAt?: Date;
    @Field({nullable: true})
    updatedAt?: Date;
}
