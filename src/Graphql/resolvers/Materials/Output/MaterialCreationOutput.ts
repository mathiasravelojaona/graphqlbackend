import {Field, ObjectType} from "type-graphql";
import {FieldError} from "../../../Types/FieldError";

@ObjectType()
export class BasicMaterialType{

    @Field()
    id: string;

    @Field()
    title: string;
}

@ObjectType()
export class MaterialCreationOutput {
    @Field(() => [FieldError], { nullable: true })
    errors?: FieldError[];

    @Field(() => BasicMaterialType, {nullable: true})
    createdMaterial: BasicMaterialType | null;
}
