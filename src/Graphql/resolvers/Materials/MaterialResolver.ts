import {Arg, Ctx, Mutation, Query, Resolver, UseMiddleware} from "type-graphql";
import {AuthMiddleware} from "../../middleware/AuthMiddleware";
import {Context} from "../../core/context";
import {MaterialCreationInput} from "./Input/MaterialCreationInput";
import {MaterialMiddleware} from "../../middleware/MaterialMiddleware";
import {MaterialCreationOutput} from "./Output/MaterialCreationOutput";
import {MaterialService} from "../../../services/common/MaterialService";
import {MaterialExceptions} from "../../../errors/MaterialExceptions";
import {MaterialUpdateInput} from "./Input/MaterialUpdateInput";
import {UserMaterialSearch} from "../../../utils/search/UserMaterialSearch";
import {MaterialListOutput} from "./Output/MaterialListOutput";
import {MaterialListInput} from "./Input/MaterialListInput";
import {MaterialDetailsOutput} from "./Output/MaterialDetailsOutput";
import {BasicMessageType} from "../../Types/BasicMessageType";


export enum MaterialSuccessMessages{
    MATERIAL_REMOVED = 'MATERIAL_REMOVED',
    MATERIAL_UPDATED = 'MATERIAL_UPDATED',
}

@Resolver()
export class MaterialResolver{

    @Mutation(() => MaterialCreationOutput)
    @UseMiddleware(AuthMiddleware.tokenMiddleware,MaterialMiddleware.materialIdMiddleware)
    async addMaterial(
        @Arg("options") options: MaterialCreationInput,
        @Ctx() context : Context<any>,
    ): Promise<MaterialCreationOutput>{
        const materialCreationOutput: MaterialCreationOutput = {
          errors: [],
          createdMaterial: null,
        };
        try{
            const resultMaterial = await MaterialService.addMaterial(
                options.title,
                options.data,
                context.userId as string,
                context.materialId as number);
            materialCreationOutput.createdMaterial = {
                title: resultMaterial.title,
                id: resultMaterial._id.toString(),
            };
        }
        catch (e){
            console.log(e);
            materialCreationOutput.errors?.push({
                field: 'data',
                message: MaterialExceptions.MATERIAL_CANNOT_BE_CREATED,
            });
        }
        return materialCreationOutput;
    }


    @Query(() => MaterialListOutput)
    @UseMiddleware(AuthMiddleware.tokenMiddleware)
    async getUserMaterials(
        @Arg("options") options: MaterialListInput,
        @Ctx() context : Context<any>,
    ): Promise<MaterialListOutput | null>{
            const results = await MaterialService.getMaterialsForUser(context.userId as string, new UserMaterialSearch(
                options.page,
                options.keyword,
                options.perceptions,
            ));
            return results as MaterialListOutput;
    }
    @Query(() => MaterialDetailsOutput)
    @UseMiddleware(AuthMiddleware.tokenMiddleware)
    async getMaterialById(@Arg('id')id: string, @Ctx() context : Context<any>): Promise<MaterialDetailsOutput>{
            const material = await MaterialService.getMaterialByIdForUser(id, context.userId as string);
            console.log(material);
            return {
                _id: material._id.toString(),
                description: material.description,
                title: material.title,
                perceptions: material.perceptionPoints,
                hd: material.hd,
                serialisation: material.serialisation,
                createdAt: material.createdAt,
                updatedAt: material.updatedAt,
            }
    }

    @Mutation(() => BasicMessageType)
    @UseMiddleware(AuthMiddleware.tokenMiddleware, MaterialMiddleware.materialIdMiddleware)
    async deleteMaterialById(@Arg('id')id: string, @Ctx() context : Context<any>): Promise<BasicMessageType>{
        try{
            const removed = await MaterialService.deleteMaterialById(id, context.userId as string);
            if(removed.deletedCount === 0){
                throw new Error(MaterialExceptions.MATERIAL_CANNOT_BE_DELETED);
            }
            return {
                message: MaterialSuccessMessages.MATERIAL_REMOVED,
                success: true,
            }
        }
        catch (e){
            return {
                message: e.message,
                success: false,
            }
        }
    }
    @Mutation(() => BasicMessageType)
    @UseMiddleware(AuthMiddleware.tokenMiddleware,MaterialMiddleware.materialByUserMiddleware, MaterialMiddleware.materialIdMiddleware)
    async updateMaterial(@Arg('options')options: MaterialUpdateInput,
                         @Ctx() context : Context<any>): Promise<BasicMessageType> {
        try {
            const material = await MaterialService.updateMaterial(
                options.id,
                context.userId as string,
                options.serialisation,
                context.materialId as number,
                options.title,
            );
            return {
                message: MaterialSuccessMessages.MATERIAL_UPDATED,
                success: true,
            }
        }
        catch (e){
            return {
                message: e.message,
                success: false,
            }
        }
    }
}
