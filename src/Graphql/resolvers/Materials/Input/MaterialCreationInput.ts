import {Field, InputType} from "type-graphql";

@InputType()
export class MaterialCreationInput {

    @Field({nullable: false})
    title: string;

    @Field({nullable: false})
    data: string;
}
