import {Field, InputType} from "type-graphql";
import {PerceptionType} from "../../../../enum/PerceptionType";

@InputType()
export class MaterialListInput {
    @Field({nullable: true})
    page: number;

    @Field({nullable: true})
    keyword: string;

    @Field(() => [PerceptionType],{nullable: true})
    perceptions: PerceptionType[];

}
