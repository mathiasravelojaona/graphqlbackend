import {Field, InputType} from "type-graphql";

@InputType()
export class MaterialUpdateInput {

    @Field({nullable: false})
    id: string;

    @Field({nullable: true})
    title?: string;

    @Field({nullable: false})
    serialisation: string;
}
