import {Arg, Ctx, Mutation, Query, Resolver, UseMiddleware} from "type-graphql";
import {AddNoteInput} from "./Input/AddNoteInput";
import {Context} from "../../core/context";
import {AuthMiddleware} from "../../middleware/AuthMiddleware";
import {MaterialMiddleware} from "../../middleware/MaterialMiddleware";
import {BasicMessageType} from "../../Types/BasicMessageType";
import {Group} from "../../../entity/models/Group";
import {GroupMiddleware} from "../../middleware/GroupMiddleware";
import {NotePresetService} from "../../../services/common/NotePresetService";
import {NotePresetMiddleware} from "../../middleware/NotePresetMiddleware";
import {NotePreset} from "../../../entity/models/NotePreset";
import {IdInput} from "../../Types/IdInput";
import {NotePresetListOutput} from "./Output/NotePresetListOutput";
import {NotePresetListInput} from "./Input/NotePresetListInput";
import {NotePresetSearch} from "../../../utils/search/NotePresetSearch";

export enum NotePresetsSuccessMessages{
    PRESET_REMOVED = 'PRESET_REMOVED',
    PRESET_UPDATED = 'PRESET_UPDATED',
    PRESET_CREATED = 'PRESET_CREATED',
}

@Resolver()
export class NotePresetsResolver {


    @Query(() => NotePreset)
    @UseMiddleware(
        AuthMiddleware.tokenMiddleware,
        NotePresetMiddleware.notePresetByIdMiddleware('id'),
    )
    async getNotePresetById(
        @Arg('params')id: IdInput,
        @Ctx() context : Context<NotePreset>,
    ): Promise<NotePreset> {
        return context.additionalData;
    }

    @Mutation(() => BasicMessageType)
    @UseMiddleware(
        AuthMiddleware.tokenMiddleware,
        MaterialMiddleware.materialIdMiddleware,
        GroupMiddleware.groupByUserMiddleware('idGroup')
    )
    async createNotePreset(
        @Arg('params') args: AddNoteInput,
        @Ctx() context : Context<Group>,
    ): Promise<BasicMessageType>{
        try{
            const data = await NotePresetService.createNotePreset(
              context.additionalData,
                args.hd,
                args.title,
                args.description,
                args.amplitudeModulationType,
                args.perceptionType,
                args.keyframeInput,
                args.originalLength,
                args.amplitudeModulationInput,
                args.carrierSignalInput,
                context.materialId as number,
            );
            return {
                message: NotePresetsSuccessMessages.PRESET_CREATED,
                success: false,
            }
        }
        catch (e){
            console.log(e);
            return {
                message: e.message,
                success: false,
            }
        }
    }

    @Query(() => NotePresetListOutput)
    @UseMiddleware(AuthMiddleware.tokenMiddleware, GroupMiddleware.groupByUserMiddleware('groupId'))
    async getNotePresets(
        @Arg('params') args: NotePresetListInput,
        @Ctx() context : Context<Group>,
    ): Promise<NotePresetListOutput>{
        const notepresetSearch = new NotePresetSearch(
            args.page,
            args.keyword,
            args.perception,
        );
        return NotePresetService.getNotePresetsForGroup(
            context.additionalData._id,
            notepresetSearch,
        );
    }
}
