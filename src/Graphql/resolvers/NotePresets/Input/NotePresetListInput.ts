
import {Field, InputType} from "type-graphql";
import {PerceptionType} from "../../../../enum/PerceptionType";

@InputType()
export class NotePresetListInput {
    @Field({nullable: false})
    page: number;

    @Field({nullable: true})
    keyword: string;

    @Field(() => PerceptionType,{nullable: false})
    perception: PerceptionType;

    @Field(() => String, {nullable: false})
    groupId: string;
}
