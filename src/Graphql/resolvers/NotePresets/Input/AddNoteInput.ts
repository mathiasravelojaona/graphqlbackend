import {Field, InputType} from "type-graphql";
import {PerceptionType} from "../../../../enum/PerceptionType";
import {KeyframePoint} from "../../../../entity/types/KeyframePoint";
import {AmplitudeModulation} from "../../../../entity/types/AmplitudeModulation";
import {CarrierSignal} from "../../../../entity/types/CarrierSignal";
import {AmplitudeModulationType} from "../../../../enum/AmplitudeModulationType";

@InputType()
export class AddNoteInput {

    @Field()
    idGroup!: string;

    @Field()
    title!: string;

    @Field()
    description!: string;

    @Field(() => Number,{nullable: false})
    amplitudeModulationType!: AmplitudeModulationType;

    @Field()
    perceptionType!: PerceptionType;

    @Field()
    hd!: boolean;

    @Field(() => [KeyframePoint], {nullable: true})
    keyframeInput!: KeyframePoint[];

    @Field()
    originalLength!: number;

    @Field(() => AmplitudeModulation, {nullable: true})
    amplitudeModulationInput: AmplitudeModulation;

    @Field(() => CarrierSignal, {nullable: true})
    carrierSignalInput: CarrierSignal;
}
