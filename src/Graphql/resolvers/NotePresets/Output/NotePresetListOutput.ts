
import BasePaginationModel from "../../../../entity/models/BasePaginationModel";
import {ObjectType} from "type-graphql";
import {NotePreset} from "../../../../entity/models/NotePreset";

@ObjectType()
export class NotePresetListOutput extends BasePaginationModel(NotePreset){}
