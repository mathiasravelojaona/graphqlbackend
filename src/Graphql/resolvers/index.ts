import {UserResolver} from "./Auth/UserResolver";
import {MaterialResolver} from "./Materials/MaterialResolver";
import {GroupResolver} from "./Group/GroupResolver";
import {NotePresetsResolver} from "./NotePresets/NotePresetsResolver";

export const Resolvers =  [
    UserResolver,
    MaterialResolver,
    GroupResolver,
    NotePresetsResolver,
]
