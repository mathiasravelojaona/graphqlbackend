import {Field, InputType} from "type-graphql";

@InputType()
export class GroupSearchInput {
    @Field({nullable: true})
    keyword: string;

    @Field()
    page!: number;
}
