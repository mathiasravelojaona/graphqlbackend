import { Field, InputType } from "type-graphql";

@InputType()
export class GroupAddInput {

    @Field({ defaultValue: false })
    shared: boolean;

    @Field({ defaultValue: '' })
    name: string;

    @Field({ defaultValue: '' })
    description: string;
}

@InputType()
export class GroupAddInputWithId extends GroupAddInput {
    @Field({ nullable: false })
    id: string
}
