import {InputType, ObjectType} from "type-graphql";
import BasePaginationModel from "../../../../entity/models/BasePaginationModel";
import {Group} from "../../../../entity/models/Group";

@ObjectType()
export class GroupSearchOutput extends BasePaginationModel(Group){}
