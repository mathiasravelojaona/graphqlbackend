import {Field, ObjectType} from "type-graphql";
import {FieldError} from "../../../Types/FieldError";


@ObjectType()
export class BasicGroupType{
    @Field()
    title!: string;

    @Field()
    shared!: boolean;

    @Field()
    id!: string;
}

@ObjectType()
export class GroupAddOutput {

    @Field(() => [FieldError], { nullable: true })
    errors?: FieldError[];

    @Field(() => BasicGroupType)
    group?: BasicGroupType | null;
}
