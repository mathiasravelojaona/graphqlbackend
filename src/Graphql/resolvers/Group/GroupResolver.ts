import {Arg, Ctx, Mutation, Query, Resolver, UseMiddleware} from "type-graphql";
import { GroupAddInput, GroupAddInputWithId } from "./Input/GroupAddInput";
import { Context } from "../../core/context";
import { AuthMiddleware } from "../../middleware/AuthMiddleware";
import { GroupAddOutput } from "./Output/GroupAddOutput";
import { GroupService } from "../../../services/common/GroupService";
import { GroupExceptions } from "../../../errors/GroupExceptions";
import {GroupSearchOutput} from "./Output/GroupSearchOutput";
import {GroupSearchInput} from "./Input/GroupSearchInput";
import {GroupSearch} from "../../../utils/search/GroupSearch";
import {BasicMessageType} from "../../Types/BasicMessageType";
import {GroupMiddleware} from "../../middleware/GroupMiddleware";
import {IdInput} from "../../Types/IdInput";
import {Group} from "../../../entity/models/Group";

export enum GroupSuccessMessages{
    GROUP_REMOVED = 'GROUP_REMOVED',
}

@Resolver()
export class GroupResolver {
    @Mutation(() => GroupAddOutput)
    @UseMiddleware(AuthMiddleware.tokenMiddleware)
    async addGroup(
        @Arg("options") options: GroupAddInput,
        @Ctx() context: Context<any>
    ): Promise<GroupAddOutput> {
        const groupAddOutput: GroupAddOutput = {
            errors: [],
            group: null,
        };
        try {
            const createdGroup = await GroupService.createGroup(options.name, options.description, context.userId!!, options.shared);
            groupAddOutput.group = {
                title: createdGroup.name,
                shared: createdGroup.shared,
                id: createdGroup._id,
            }
        }
        catch (e) {
            console.log(e);
            groupAddOutput.errors = [
                { field: 'group', message: GroupExceptions.CANNOT_CREATE_GROUP },
            ]
        }
        return groupAddOutput;
    }

    @Mutation(() => GroupAddOutput)
    @UseMiddleware(AuthMiddleware.tokenMiddleware)
    async updateGroup(
        @Arg('options') options: GroupAddInputWithId,
        @Ctx() context: Context<any>
    ): Promise<GroupAddOutput> {
        const groupAddOutput: GroupAddOutput = {
            errors: [],
            group: null,
        };
        try {
            const createdGroup = await GroupService.updateGroup(options.id, {
                description: options.description,
                shared: options.shared,
                name: options.name,
            });
            groupAddOutput.group = {
                title: createdGroup.name,
                shared: createdGroup.shared,
                id: createdGroup._id,
            }
        }
        catch (e) {
            groupAddOutput.errors = [
                { field: 'group', message: GroupExceptions.CANNOT_UPDATE_GROUP },
            ]
        }
        return groupAddOutput;
    }

    @Query(() => GroupSearchOutput)
    @UseMiddleware(AuthMiddleware.tokenMiddleware)
    async getGroups(
        @Arg('params') options: GroupSearchInput,
        @Ctx() context: Context<any>
    ): Promise<GroupSearchOutput>{
        return GroupService.searchGroups(context.userId as string,new GroupSearch(options.page,options.keyword));
    }

    @Mutation(() => BasicMessageType)
    @UseMiddleware(AuthMiddleware.tokenMiddleware, GroupMiddleware.groupByUserMiddleware('id'))
    async deleteGroup(
        @Arg('params') options: IdInput,
        @Ctx() context: Context<Group>
    ): Promise<BasicMessageType>{
        try{
            await GroupService.deleteGroup(options.id);
            return {
                success: true,
                message: GroupSuccessMessages.GROUP_REMOVED,
            };
        }
        catch (e){
            return {
                success: true,
                message: e.message,
            };
        }
    }
}
