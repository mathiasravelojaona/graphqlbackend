import {Field, ObjectType} from "type-graphql";
import {FieldError} from "../../../Types/FieldError";
import {User} from "../../../../entity/models/User";

@ObjectType()
export class UserResponse {
    @Field(() => [FieldError], { nullable: true })
    errors?: FieldError[];

    @Field(() => User, { nullable: true })
    user?: User;
}
