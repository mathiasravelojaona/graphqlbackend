import {Field, ObjectType} from "type-graphql";
import {FieldError} from "../../../Types/FieldError";

@ObjectType()
export class ProfileType {
    @Field()
    name: string;
}

@ObjectType()
export class TokenResponse {
    @Field(() => [FieldError], { nullable: true })
    errors?: FieldError[];

    @Field({ nullable: true })
    token: string;

    @Field(() => ProfileType, {nullable: true})
    user: ProfileType;
}
