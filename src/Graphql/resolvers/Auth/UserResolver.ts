import {
  Arg,
  Ctx,
  Mutation,
  Query,
  Resolver, UseMiddleware,
} from "type-graphql";
import {Context} from "../../core/context";
import {UsernamePasswordInput} from "./Input/UsernamePasswordInput";
import {AuthService} from "../../../services/common/AuthService";
import {TokenResponse} from "./Output/TokenResponse";
import {User, UserWithId} from "../../../entity/models/User";
import {AuthMiddleware} from "../../middleware/AuthMiddleware";
import * as mongoose from "mongoose";

@Resolver()
export class UserResolver {
  @Query(() => String)
  @UseMiddleware(AuthMiddleware.tokenMiddleware)
  async me(
      @Ctx() context : Context<any>
  ): Promise<string>{
    const user = await AuthService.getProfile(context.userId as string);
    return user.email;
  }

  @Mutation(() => TokenResponse)
  async register(
    @Arg("options") options: UsernamePasswordInput,
    @Ctx() context : Context<any>
  ): Promise<TokenResponse> {
    let user : UserWithId = await AuthService.registerUser(options.email,options.password);
    return {
      token: await AuthService.createToken(user._id.toString()),
      user: {
        name: user.email,
      }
    };
  }

  @Mutation(() => TokenResponse)
  async login(
    @Arg("options") options: UsernamePasswordInput,
    @Ctx() {req}: any
  ): Promise<TokenResponse> {
    let user : UserWithId = await AuthService.login(options.email,options.password);
    //store user id session, auto-logs in after registration🤩
    return {
      token: await AuthService.createToken(user._id.toString()),
      user: {
        name: user.email,
      }
    };
  }
}
