import {Field, ObjectType} from "type-graphql";

@ObjectType()
export class BasicMessageType {
    @Field({defaultValue: ''})
    message: string;

    @Field({nullable: false})
    success: boolean;
}
