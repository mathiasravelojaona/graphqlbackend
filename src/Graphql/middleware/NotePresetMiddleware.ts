import {MiddlewareFn} from "type-graphql";
import {Context} from "../core/context";
import {Group} from "../../entity/models/Group";
import {NotePresetService} from "../../services/common/NotePresetService";
import {NotePreset} from "../../entity/models/NotePreset";
import {CoreExceptions} from "../../errors/CoreExceptions";

export class NotePresetMiddleware {
    public static notePresetByIdMiddleware= (tag: string): MiddlewareFn<Context<NotePreset>>  => {
        const middleware: MiddlewareFn<Context<NotePreset>> =
            async ({context, args}: { context: Context<NotePreset>, args: any }, next) => {
                const notePreset = await NotePresetService.getNotePresetByIdForUser(args.params[tag], context.userId as string);
                if(!notePreset){
                    throw new Error(CoreExceptions.ENTITY_NOT_FOUND);
                }
                context.additionalData = notePreset;
                await next();
            };
        return middleware;
    }
}
