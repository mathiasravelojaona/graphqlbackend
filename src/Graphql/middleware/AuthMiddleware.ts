import {MiddlewareFn} from "type-graphql";
import {Context} from "../core/context";
import {AuthExceptions} from "../../errors/AuthExceptions";
import {RedisService} from "../../services/common/RedisService";
import {JwtService} from "../../utils/services/JwtService";
import {AuthService} from "../../services/common/AuthService";


export class AuthMiddleware {
    public static tokenMiddleware: MiddlewareFn<Context<any>> = async ({context}: {context: Context<any>}, next) => {
        const token = context.request.headers.authorization;
        if(token){
            const isValidToken = JwtService.isValidToken(token);
            if(!isValidToken){
                throw new Error(AuthExceptions.TOKEN_NOT_VALID);
            }
            const user = await RedisService.getValue(token);
            if(!user){
                throw new Error(AuthExceptions.TOKEN_NOT_VALID);
            }
            let userData = await AuthService.getProfile(user);
            if(!userData){
                throw new Error(AuthExceptions.USER_NOT_FOUND);
            }
            context.userId = user;
        }
        else{
            throw new Error(AuthExceptions.TOKEN_NOT_FOUND);
        }
        await next();
    }
}
