import {MiddlewareFn} from "type-graphql";
import {Context} from "../core/context";
import {NativeMaterialService} from "../../services/native/NativeMaterialService";
import {MaterialService} from "../../services/common/MaterialService";
import {Material} from "../../entity/models/Material";
import {MaterialExceptions} from "../../errors/MaterialExceptions";

export class MaterialMiddleware {
    public static  materialIdMiddleware: MiddlewareFn<Context<any>> = async ({context}: {context: Context<any>}, next) =>  {
        const id = NativeMaterialService.createMaterial();
        context.materialId = id;
        await next();
        NativeMaterialService.removeMaterial(id);
    }
    public static materialByUserMiddleware: MiddlewareFn<Context<Material>> =
        async ({context, args}: {context: Context<Material>, args: any & {id: string}}, next) =>  {
        const material = await MaterialService.getMaterialById(args.id);
        if(!material){
            throw new Error(MaterialExceptions.MATERIAL_NOT_FOUND);
        }
        context.additionalData = material;
        await next();
    }
}
