import {MiddlewareFn} from "type-graphql";
import {Context} from "../core/context";
import {GroupService} from "../../services/common/GroupService";
import {GroupExceptions} from "../../errors/GroupExceptions";
import {Group} from "../../entity/models/Group";

export class GroupMiddleware {
    public static groupByUserMiddleware= (tag: string): MiddlewareFn<Context<Group>>  => {
        const middleware: MiddlewareFn<Context<Group>> =
            async ({context, args}: { context: Context<Group>, args: any }, next) => {
                const group = await GroupService.getGroupById(args.params[tag], context.userId as string);
                if(!group){
                    throw new Error(GroupExceptions.GROUP_NOT_FOUND);
                }
                context.additionalData = group;
                await next();
            };
        return middleware;
    }
}
