import Redis from "ioredis";
export const redis = new Redis({
    port: parseInt(process.env.REDIS_PORT as string),
    host: process.env.REDIS_HOST,
    password: process.env.REDIS_PASSWORD
});
