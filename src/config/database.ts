import { connect} from "mongoose";

const connectDB = async () => {
  try {
    const mongoURI: string = process.env.MONGO_HOST as string;
    await connect(mongoURI);
  } catch (err) {
    console.error(err.message);
    // Exit process with failure
    process.exit(1);
  }
};

export default connectDB;
