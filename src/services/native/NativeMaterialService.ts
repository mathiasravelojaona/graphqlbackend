import {calls} from "./calls";
import {MaterialExceptions} from "../../errors/MaterialExceptions";

export class NativeMaterialService {
    public static createMaterial(): number{
        const id = calls.AddHMaterial();
        if(id < 0){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
        return id;
    }
    public static removeMaterial(id: number){
        const result = calls.RemoveHMaterial(id);
        if(!result){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
    }
    public static loadMaterialAtIndex(id: number, serialisation: string){
        const result = calls.UpdateHMaterial(serialisation,id);
        if(!result){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
    }
    public static setHd(materialId: number,hd: boolean){
        const hdFlag = calls.GetHDFlag(materialId);
        if(hd !== hdFlag){
            const result = calls.SetHDFlag(materialId, hd);
            if(!result){
                throw new Error(MaterialExceptions.NATIVE_ERROR);
            }
        }
    }
    public static getHd(materialId: number){
        const hdFlag = calls.GetHDFlag(materialId);
        return hdFlag;
    }
}
