import {Point} from "../../entity/types/Point";
import {PerceptionType} from "../../enum/PerceptionType";
import {calls, createDoubleArray} from "./calls";
import {MaterialExceptions} from "../../errors/MaterialExceptions";
import {EvaluateMax, EvaluateSteps} from "../../enum/EvaluateSteps";
import {Domain} from "../../utils/types/maths/Domain";
import {MathService} from "../common/MathService";
import {NativeNoteService} from "./NativeNoteService";
import {NativeAnimationCurveService} from "./NativeAnimationCurveService";

export class NativeEvaluateService {
    public static evaluatePerception(idmaterial: number, idperception: number): Point[]{
        const domaine = new Domain(0, EvaluateMax.MATERIAL);
        let step = EvaluateSteps.MATERIAL;
        if(idperception === PerceptionType.STIFFNESS){
            step = EvaluateSteps.STIFFNESS;
            domaine.superiorLimit = EvaluateMax.STIFFNESS;
        }
        const points = MathService.domainToPoints(domaine, step);
        const results = NativeEvaluateService.evaluateMaterialPack(idmaterial, idperception, points);
        return points.map((value, index) => ({
            x: value,
            y: results[index],
        }));
    }
    private static evaluateMaterialPack(
        idmaterial: number,
        idperception: PerceptionType,
        domain: number[]
    ): number[] {
        const domains = createDoubleArray(domain);
        const results = createDoubleArray(domain.length);
        const resultatEvaluate = calls.EvaluatePack(
            idmaterial,
            idperception,
            domains,
            results,
            domain.length
        );
        if (!resultatEvaluate) {
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
        return results.toArray() as number[];
    }
    public static evaluateCarrierSignal(idmaterial: number,
                               idperception: number,
                               idmelody: number,
                               idnote: number): Point[]{
        const domain = new Domain(0, NativeNoteService.getLength(
            idmaterial,
            idperception,
            idmelody,
            idnote,
        ));
        const points = MathService.domainToPoints(domain, EvaluateSteps.CARRIER_SIGNAL);
        const results = NativeEvaluateService.evaluateUnderlyingSignalPack(idmaterial, idperception, idmelody, idnote, points);
        return points.map((value, index) => ({
            x: value,
            y: results[index],
        }));
    }
    public static evaluateAmplitudeModulation(
        idmaterial: number,
        idperception: number,
        idmelody: number,
        idnote: number
    ): Point[]{
        const length = NativeNoteService.getLength(idmaterial, idperception, idmelody, idnote);
        let points = MathService.domainToPoints(new Domain(0, length), EvaluateSteps.AMPLITUDE_MODULATION);
        const keyframePositions = NativeAnimationCurveService.GetKeyFramesXPosition(
            idmaterial,
            idperception,
            idmelody,
            idnote
        );
        keyframePositions.forEach(value => {
            if (!points.includes(value)) {
                points.push(value);
            }
        });
        points = points.sort((a, b) => a - b);
        const resultsEvaluate = NativeEvaluateService.evaluateAmplitudeModulationPack(
            idmaterial,
            idperception,
            idmelody,
            idnote,
            points
        );
        return points.map((value, index) => ({
            y: resultsEvaluate[index],
            x: value,
        }));
    }
    public static evaluateAmplitudeModulationPack(
        idmaterial: number,
        idperception: PerceptionType,
        idmelody: number,
        idnote: number,
        domain: number[]
    ): number[] {
        const domains = createDoubleArray(domain);
        const results = createDoubleArray(domain.length);
        const resultatEvaluate = calls.EvaluateAmplitudeModulationPack(
            idmaterial,
            idperception,
            idmelody,
            idnote,
            domains,
            results,
            domain.length
        );
        if (!resultatEvaluate) {
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
        return results.toArray() as number[];
    }
    public static evaluateResultNote(idmaterial: number,
                                     idperception: number,
                                     idmelody: number,
                                     idnote: number){
        const length = NativeNoteService.getLength(idmaterial, idperception, idmelody, idnote);
        const points = MathService.domainToPoints(new Domain(0, length), EvaluateSteps.RESULT_NOTE);
        const results = NativeEvaluateService.evaluateNotePack(
            idmaterial,
            idperception,
            idmelody,
            idnote,
            points
        );
        return points.map((value, index) => ({
            x: value,
            y: results[index],
        }));
    }
    private static evaluateNotePack(
        idmaterial: number,
        idperception: PerceptionType,
        idmelody: number,
        idnote: number,
        domain: number[]
    ): number[] {
        const domains = createDoubleArray(domain);
        const results = createDoubleArray(domain.length);
        const resultatEvaluate = calls.EvaluateNotePack(
            idmaterial,
            idperception,
            idmelody,
            idnote,
            domains,
            results,
            domain.length
        );
        if (!resultatEvaluate) {
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
        return results.toArray() as number[];
    }
    public static evaluateUnderlyingSignalPack(
        idmaterial: number,
        idperception: PerceptionType,
        idmelody: number,
        idnote: number,
        domain: number[]
    ): number[] {
        const domains = createDoubleArray(domain);
        const results = createDoubleArray(domain.map(value => 0));
        const resultatEvaluate = calls.EvaluateUnderlyingSignalPack(
            idmaterial,
            idperception,
            idmelody,
            idnote,
            domains,
            results,
            domain.length
        );
        if (!resultatEvaluate) {
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
        return results.toArray() as number[];
    }
}
