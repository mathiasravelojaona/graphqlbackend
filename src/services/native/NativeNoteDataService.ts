import {calls} from "./calls";
import {PerceptionType} from "../../enum/PerceptionType";
import {MaterialExceptions} from "../../errors/MaterialExceptions";
import {PatternFunctions} from "../../enum/PatternFunctions";
import {AmplitudeModulationType} from "../../enum/AmplitudeModulationType";
import {CarrierSignal} from "../../entity/types/CarrierSignal";
import {AmplitudeModulation} from "../../entity/types/AmplitudeModulation";
import {NativeEvaluateService} from "./NativeEvaluateService";

export class NativeNoteDataService {
    public static setFrequency(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
        frequency: number
    ) {
        const result = calls.SetFrequency(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
            frequency
        );
        if(!result){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
    }
    public static getFrequency(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
    ) {
        const result = calls.GetFrequency(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
        );
        return result;
    }
    public static setPattern(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
        pattern: PatternFunctions
    ) {
        const result = calls.SetSignalType(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
            pattern
        );
        if(!result){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
    }
    public static getPattern(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
    ) {
        const result = calls.GetSignalType(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
        );
        return result;
    }
    public static setOffset(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
        offset: number,
    ) {
        const result = calls.SetVerticalOffset(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
            offset
        );
        if(!result){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
    }
    public static getOffset(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
    ) {
        const result = calls.GetVerticalOffset(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
        );
        return result;
    }
    public static setPhase(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
        phase: number,
    ) {
        const result = calls.SetPhase(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
            phase
        );
        if(!result){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
    }
    public static getPhase(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
    ) {
        const result = calls.GetPhase(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
        );
        return result;
    }
    public static setAmplitude(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
        amplitude: number
    ) {
        const result = calls.SetAmplitude(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
            amplitude
        );
        if(!result){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
    }
    public static getAmplitude(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        isAmplitudeModulation: boolean,
    ) {
        const result = calls.GetAmplitude(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            isAmplitudeModulation,
        );
        return result;
    }
    public static setModulationType(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
        modulationType: AmplitudeModulationType,
    ) {
        if(modulationType === calls.GetAmplitudeModulationType(
            materialId,
            perceptionId,
            melodyId,
            idNote,
        )){
            return;
        }
        const result = calls.SetAmplitudeModulationType(
            materialId,
            perceptionId,
            melodyId,
            idNote,
            modulationType,
        );
        if(!result){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
    }
    public static getModulationType(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
    ): AmplitudeModulationType{
        return calls.GetAmplitudeModulationType(
            materialId,
            perceptionId,
            melodyId,
            idNote,
        );
    }
    public static getCarrierSignal(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
    ): CarrierSignal{
        return {
          pattern: NativeNoteDataService.getPattern(
              materialId,
              perceptionId,
              melodyId,
              idNote,
              false,
          ),
            frequency:NativeNoteDataService.getFrequency(
                materialId,
                perceptionId,
                melodyId,
                idNote,
                false,
            ),
            curve: NativeEvaluateService.evaluateCarrierSignal(
                materialId,
                perceptionId,
                melodyId,
                idNote,
            ),
        };
    };
    public static getAmplitudeModulation(
        materialId: number,
        perceptionId: PerceptionType,
        melodyId: number,
        idNote: number,
    ): AmplitudeModulation{
        return {
            pattern: NativeNoteDataService.getPattern(
                materialId,
                perceptionId,
                melodyId,
                idNote,
                true,
            ),
            frequency:NativeNoteDataService.getFrequency(
                materialId,
                perceptionId,
                melodyId,
                idNote,
                true,
            ),
            amplitude: NativeNoteDataService.getAmplitude(
                materialId,
                perceptionId,
                melodyId,
                idNote,
                true,
            ),
            offset: NativeNoteDataService.getOffset(
                materialId,
                perceptionId,
                melodyId,
                idNote,
                true,
            ),
            phase: NativeNoteDataService.getPhase(
                materialId,
                perceptionId,
                melodyId,
                idNote,
                true,
            ),
        };
    }
}
