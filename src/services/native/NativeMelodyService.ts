import {calls} from "./calls";
import {MelodyException} from "../../errors/MelodyException";

export class NativeMelodyService {
    public static addMelody(materialId: number, perceptionId: number): number{
        const result = calls.AddMelody(materialId,perceptionId);
        if(result < 0){
            throw new Error(MelodyException.CANNOT_CREATE_MELODY);
        }
        return result;
    }
}
