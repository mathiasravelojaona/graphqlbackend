import {calls} from "./calls";
import {KeyFrame} from "../../entity/types/KeyFrame";
import {Point} from "../../entity/types/Point";
import {MathService} from "../common/MathService";
import {MaterialExceptions} from "../../errors/MaterialExceptions";
import {NativeNoteService} from "./NativeNoteService";

export class NativeAnimationCurveService {
    public static getInSlope(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        keyFrameId: number
    ) {
        return calls.GetInSlope(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            keyFrameId
        ) as number;
    }
    public static getOutSlope(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        keyFrameId: number
    ) {
        return calls.GetOutSlope(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            keyFrameId
        ) as number;
    }
    public static getKeyFrameTime(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        keyFrameId: number
    ) {
        return calls.GetTime(materialId, perceptionIndex, melodyIndex, noteIndex, keyFrameId) as number;
    }
    public static getKeyFrameAmplitude(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        keyFrameId: number
    ) {
        return calls.GetValue(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            keyFrameId
        ) as number;
    }
    public static   getKeyframeNumbers(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number
    ): number{
        return calls.GetAnimationCurveSize(materialId, perceptionIndex, melodyIndex, noteIndex);
    }
    public static getKeyFrameById(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        keyFrameId: number
    ): KeyFrame{
        const inSlope = NativeAnimationCurveService.getInSlope(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            keyFrameId
        );
        const outSlope = NativeAnimationCurveService.getInSlope(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            keyFrameId
        );
        const origin: Point = {
            x: NativeAnimationCurveService.getKeyFrameTime(
                materialId,
                perceptionIndex,
                melodyIndex,
                noteIndex,
                keyFrameId
            ),
            y: NativeAnimationCurveService.getKeyFrameAmplitude(
                materialId,
                perceptionIndex,
                melodyIndex,
                noteIndex,
                keyFrameId
            ),
        };
        return {
            id: keyFrameId,
            materialId,
            perceptionId: perceptionIndex,
            melodyId: melodyIndex,
            noteId: noteIndex,
            coordinate: {
                x: origin.x,
                y: origin.y,
            },
            tangent: inSlope,
            tangentCurve: [],
            slopeAngle: MathService.slopeToAngle(inSlope),
        }
    };
    public static getAllKeyFrames(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number
    ): KeyFrame[]{
        const keyframeNumbers = NativeAnimationCurveService.getKeyframeNumbers(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex
        );
        const keyframeArray: KeyFrame[] = [];
        for (let i = 0; i < keyframeNumbers; i++) {
            keyframeArray.push(
                NativeAnimationCurveService.getKeyFrameById(
                materialId,
                perceptionIndex,
                melodyIndex,
                noteIndex,
                i)
            );
        }
        return keyframeArray;
    }
    public static setKeyFrameTime(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        idKeyframe: number,
        time: number
    ): boolean {
        const result = calls.SetTime(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            idKeyframe,
            time
        );
        if (!result) {
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
        return result;
    }
    public static setKeyFrameAmplitude(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        idKeyframe: number,
        amplitude: number
    ): boolean {
        const result = calls.SetValue(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            idKeyframe,
            amplitude
        );
        if (!result) {
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
        return result;
    }
    public static moveKeyFrame(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        idKeyframe: number,
        amplitude: number,
        time: number,
        tangent: number,
    ) {
        const resultSetTime = NativeAnimationCurveService.setKeyFrameTime(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            idKeyframe,
            time
        );
        if (!resultSetTime) {
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
        const resultSetValue = NativeAnimationCurveService.setKeyFrameAmplitude(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            idKeyframe,
            amplitude
        );
        NativeAnimationCurveService.setTangent(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            perceptionIndex,
            tangent,
        );
        return resultSetValue;
    }
    public static GetKeyFramesXPosition(
        materialId: number,
        perceptionId: number,
        melodyId: number,
        noteId: number
    ): number[]{
    const keyframeNumbers = NativeAnimationCurveService.getKeyframeNumbers(
        materialId,
        perceptionId,
        melodyId,
        noteId
    );
    const keyframeArray: number[] = [];
    for (let i = 0; i < keyframeNumbers; i++) {
        keyframeArray.push(
            NativeAnimationCurveService.getKeyFrameTime(materialId, perceptionId, melodyId, noteId, i)
        );
    }
    return keyframeArray;
    }
    public static addKeyFrame(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        y: number,
        x: number,
        slope: number,
    ){
        const keyFrameId: number = calls.AddKeyFrameAtTimer(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            y,
            x,
        );
        NativeAnimationCurveService.setTangent(
            materialId,
            perceptionIndex,
            melodyIndex,
            noteIndex,
            keyFrameId,
            slope,
        );
    }
    public static setTangent(
        materialId: number,
        perceptionIndex: number,
        melodyIndex: number,
        noteIndex: number,
        keyFrameId: number,
        tgt: number
    ){
    const result = calls.SetInOutSlope(
        materialId,
        perceptionIndex,
        melodyIndex,
        noteIndex,
        keyFrameId,
        tgt,
        tgt
    );
    if (!result) {
        throw new Error(MaterialExceptions.NATIVE_ERROR);
    }
    return result;
    }
}
