const ffi = require('ffi-napi');
const ref = require('ref-napi');
const ArrayType = require('ref-array-napi');

const int = ref.types.int;
const IntArray = ArrayType(int);
const double = ref.types.double;
const DoubleArray = ArrayType(double);

export const calls = ffi.Library('composer', {
  /**
   * Material Calls
   */
  LoadHMaterial: [ref.types.int, [ref.types.CString]],
  UpdateHMaterial: [ref.types.bool, [ref.types.CString, ref.types.int]],
  SaveHMaterial: [ref.types.int, [ref.types.int, 'byte*', ref.types.int]],
  AddPerception: [ref.types.bool, [ref.types.int, ref.types.int]],
  RemovePerception: [ref.types.bool, [ref.types.int, ref.types.int]],
  GetHMaterialPerceptionsStatus: [ref.types.CString, [ref.types.int]],
  AddHMaterial: ['int', []],
  RemoveHMaterial: [ref.types.bool, [ref.types.int]],
  GetHMaterialsSize: [ref.types.int, []],
  GetHMaterialDescription: [ref.types.int, [ref.types.int, 'byte*', ref.types.int]],
  SetHMaterialDescription: [ref.types.bool, [ref.types.int, ref.types.CString]],
  GetHDFlag: [ref.types.bool, [ref.types.int]],
  SetHDFlag: [ref.types.bool, [ref.types.int, ref.types.bool]],
  SetLengthUnit: [ref.types.bool, [ref.types.int, ref.types.int]],
  GetLengthUnit: [ref.types.int, [ref.types.int]],
  GetTimeUnit: [ref.types.int, [ref.types.int]],
  SetTimeUnit: [ref.types.bool, [ref.types.int, ref.types.int]],
  GetHMaterialVolume: [ref.types.double, [ref.types.int]],
  SetHMaterialVolume: [ref.types.bool, [ref.types.int, ref.types.double]],
  GetMelodiesSize: [ref.types.int, [ref.types.int, ref.types.int]],
  AddMelody: [ref.types.int, [ref.types.int, ref.types.int]],
  RemoveMelody: [ref.types.bool, [ref.types.int, ref.types.int, ref.types.int]],
  GetMelodySize: [ref.types.int, [ref.types.int, ref.types.int, ref.types.int]],
  SwapNotes: [
    ref.types.bool,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  MoveNotes: [
    ref.types.bool,
    [ref.types.int, ref.types.int, IntArray, IntArray, IntArray, DoubleArray, IntArray, ref.types.int],
  ],
  /**
   * Melody calls
   */
  CreateNoteAt: [ref.types.int, [ref.types.int, ref.types.int, ref.types.int, ref.types.double]],
  RemoveNoteAt: [ref.types.bool, [ref.types.int, ref.types.int, ref.types.int, ref.types.int]],
  GetMelodyVolume: [ref.types.double, [ref.types.int, ref.types.int, ref.types.int]],
  SetMelodyVolume: [
    ref.types.bool,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.double],
  ],
  GetMuteValue: [ref.types.bool, [ref.types.int, ref.types.int, ref.types.int]],
  SetMuteValue: [ref.types.bool, [ref.types.int, ref.types.int, ref.types.int, ref.types.int]],
  /**
   * Note calls
   */
  SetLength: [
    ref.types.bool,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.double],
  ],
  GetLength: [ref.types.double, [ref.types.int, ref.types.int, ref.types.int, ref.types.int]],
  GetAmplitudeModulationType: [
    ref.types.int,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  SetAmplitudeModulationType: [
    ref.types.bool,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  ResetUnderlyingSignal: [
    ref.types.bool,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  MoveNote: [
    ref.types.int,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.double],
  ],
  GetStartingPoint: [
    ref.types.double,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  /**
   * Animation curve calls
   */
  AddKeyFrameAtTimer: [
    ref.types.int,
    [
      ref.types.int,
      ref.types.int,
      ref.types.int,
      ref.types.int,
      ref.types.double,
      ref.types.double,
    ],
  ],
  RemoveKeyFrame: [
    ref.types.bool,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  SwapKeyFrame: [
    ref.types.bool,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  GetAnimationCurveSize: [
    ref.types.int,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  /**
   * Keyframe
   */
  SetTime: [
    'bool',
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.double],
  ],
  GetTime: ['double', [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int]],
  SetValue: [
    'bool',
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.double],
  ],
  GetValue: ['double', [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int]],
  SetInOutSlope: [
    'bool',
    [
      ref.types.int,
      ref.types.int,
      ref.types.int,
      ref.types.int,
      ref.types.int,
      ref.types.double,
      ref.types.double,
    ],
  ],
  GetInSlope: [
    'double',
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  GetOutSlope: [
    'double',
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.int],
  ],
  /**
   * PeriodicSignal calls
   */
  SetFrequency: ['bool', ['int', 'int', 'int', 'int', 'bool', 'double']],
  GetFrequency: ['double', ['int', 'int', 'int', 'int', 'bool']],
  SetAmplitude: ['bool', ['int', 'int', 'int', 'int', 'bool', 'double']],
  GetAmplitude: ['double', ['int', 'int', 'int', 'int', 'bool']],
  SetPhase: ['bool', ['int', 'int', 'int', 'int', 'bool', 'double']],
  GetPhase: ['double', ['int', 'int', 'int', 'int', 'bool']],
  SetVerticalOffset: ['bool', ['int', 'int', 'int', 'int', 'bool', 'double']],
  GetVerticalOffset: ['double', ['int', 'int', 'int', 'int', 'bool']],
  SetSignalType: ['bool', ['int', 'int', 'int', 'int', 'bool', 'int']],
  GetSignalType: ['int', ['int', 'int', 'int', 'int', 'bool']],

  /**
   * Evaluate
   */
  Evaluate: [ref.types.double, [ref.types.int, ref.types.int, ref.types.double]],
  EvaluateNote: [
    ref.types.double,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.double],
  ],
  EvaluateAmplitudeModulation: [
    ref.types.double,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.double],
  ],
  EvaluateUnderlyingSignal: [
    ref.types.double,
    [ref.types.int, ref.types.int, ref.types.int, ref.types.int, ref.types.double],
  ],
  SetSignalEvaluationMethod: [ref.types.bool, [ref.types.int, ref.types.int, ref.types.int]],
  GetSignalEvaluationMethod: [ref.types.int, [ref.types.int, ref.types.int]],

  /**
   * Evaluate pack
   */

  EvaluatePack: ['bool', ['int', 'int', DoubleArray, DoubleArray, 'int']],
  EvaluateNotePack: [
    ref.types.bool,
    [
      ref.types.int,
      ref.types.int,
      ref.types.int,
      ref.types.int,
      DoubleArray,
      DoubleArray,
      ref.types.int,
    ],
  ],
  EvaluateAmplitudeModulationPack: [
    ref.types.bool,
    [
      ref.types.int,
      ref.types.int,
      ref.types.int,
      ref.types.int,
      DoubleArray,
      DoubleArray,
      ref.types.int,
    ],
  ],
  EvaluateUnderlyingSignalPack: [
    ref.types.bool,
    [
      ref.types.int,
      ref.types.int,
      ref.types.int,
      ref.types.int,
      DoubleArray,
      DoubleArray,
      ref.types.int,
    ],
  ],
});

export const createDoubleArray = (data: number[] | number) => {
  return DoubleArray(data);
};

export const createIntArray = (data: number[] | number) => {
  return IntArray(data);
}
