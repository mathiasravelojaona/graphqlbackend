import {calls} from "./calls";
import {NoteException} from "../../errors/NoteException";
import {MaterialExceptions} from "../../errors/MaterialExceptions";
import {MaterialService} from "../common/MaterialService";
import {PerceptionType} from "../../enum/PerceptionType";
import {CarrierSignal} from "../../entity/types/CarrierSignal";
import {NativeNoteDataService} from "./NativeNoteDataService";
import {Point} from "../../entity/types/Point";
import {NativeEvaluateService} from "./NativeEvaluateService";
import {AmplitudeModulation} from "../../entity/types/AmplitudeModulation";

export class NativeNoteService {
    public static addNote(idMaterial: number,
                          idPerception: number,
                          idMelody: number,
                          time: number): number{
        const result  = calls.CreateNoteAt(idMaterial,idPerception,idMelody,time);
        if(result < 0 ){
            throw new Error(NoteException.CANNOT_ADD_NOTE);
        }
        return result;
    }
    public static setLength(
        idMaterial: number,
        idPerception: number,
        idMelody: number,
        idNote: number,
        length: number,
    ){
        const setLengthSucceed: boolean = calls.SetLength(
            idMaterial,
            idPerception,
            idMelody,
            idNote,
            length
        );
        if(!setLengthSucceed){
            throw new Error(MaterialExceptions.NATIVE_ERROR);
        }
    }
    public static getLength(
        idMaterial: number,
        idPerception: number,
        idMelody: number,
        idNote: number,
    ): number{
        const length: number = calls.GetLength(
            idMaterial,
            idPerception,
            idMelody,
            idNote,
        );
        return length;
    }

    public static applyCarrierSignal(
        placementData: {materialId: number, perceptionId: PerceptionType, melodyId: number,noteId: number},
        carrierSignal: CarrierSignal
    ){
        NativeNoteDataService.setFrequency(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
            false,
            carrierSignal.frequency,
        )
        NativeNoteDataService.setPattern(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
            false,
            carrierSignal.pattern,
        );
    }
    public static applyAmplitudeModulation(
        placementData: {materialId: number, perceptionId: PerceptionType, melodyId: number,noteId: number},
        amplitudeModulation: AmplitudeModulation,
    ){
        NativeNoteDataService.setFrequency(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
            true,
            amplitudeModulation.frequency,
        )
        NativeNoteDataService.setPattern(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
            true,
            amplitudeModulation.pattern,
        );
        NativeNoteDataService.setAmplitude(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
            true,
            amplitudeModulation.amplitude,
        );
        NativeNoteDataService.setPhase(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
            true,
            amplitudeModulation.phase,
        );
        NativeNoteDataService.setOffset(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
            true,
            amplitudeModulation.offset,
        );
    }
}
