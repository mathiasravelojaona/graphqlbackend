import {PerceptionType} from "../../enum/PerceptionType";
import {calls} from "./calls";
import {PerceptionException} from "../../errors/PerceptionException";

export class NativePerceptionService {
    public static addPerception(materialId: number, perceptionId: PerceptionType){
        const result = calls.AddPerception(materialId,perceptionId);
        if(!result){
            throw new Error(PerceptionException.CANNOT_ADD_PERCEPTION);
        }
    }
}
