import Big from "big.js";
import {Domain} from "../../utils/types/maths/Domain";
import {Point} from "../../entity/types/Point";
import { angleToDegrees, angleToRadians, Point as geometricPoint, pointRotate } from 'geometric';
export class MathService {
    public static domainToPoints(domain: Domain, step: number): number[] {
        let position: Big = new Big(domain.inferiorLimit);
        const points: Big[] = [];
        while (position.lte(domain.superiorLimit)) {
            points.push(position);
            position = position.plus(step);
        }
        if (points.filter(value => value.eq(domain.superiorLimit)).length === 0) {
            points.push(new Big(domain.superiorLimit));
        }
        return points.map(value => value.toNumber());
    }
    public static rotatePoint(origin: Point, angle: number, point: Point): Point {
        const radians = (Math.PI / 180) * angle;
        const cos = Math.cos(radians);
        const sin = Math.sin(radians);
        const nx = cos * (point.x - origin.x) + sin * (point.y - origin.y) + origin.x;
        const ny = cos * (point.y - origin.y) - sin * (point.x - origin.x) + origin.y;
        return {
            x: nx,
            y: ny,
        };
    }
    public static slopeToAngle(slope: number): number {
        return angleToDegrees(Math.atan(slope));
    }
    /**
     * translates a point through an angle
     * @param origine
     * @param slope
     */
    public static getTranslationCurve(origine: Point, slope: number, length: number): Point[] {
        const angle = MathService.slopeToAngle(slope);
        const leftPoint = MathService.rotatePoint(origine, -angle, {
            x: origine.x - length,
            y: origine.y,
        });
        const rightPoint = MathService.rotatePoint(origine, -angle, {
            x: origine.x + length,
            y: origine.y,
        });
        return [
            {
                x: leftPoint.x,
                y: leftPoint.y,
            },
            {
                x: rightPoint.x,
                y: rightPoint.y,
            },
        ];
    }
}
