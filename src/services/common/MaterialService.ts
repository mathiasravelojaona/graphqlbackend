import MaterialModel, {Material} from "../../entity/models/Material";
import {SerializedMaterial} from "../../utils/types/serialization/serializedMaterial";
import {PerceptionType} from "../../enum/PerceptionType";
import {PerceptionWithOutputType} from "../../entity/types/PerceptionWithOutputType";
import {NativeMaterialService} from "../native/NativeMaterialService";
import {NativeEvaluateService} from "../native/NativeEvaluateService";
import simplify from "simplify-js";
import {UserMaterialSearch} from "../../utils/search/UserMaterialSearch";
import {mongoose} from "@typegoose/typegoose";
import {KeyframePoint} from "../../entity/types/KeyframePoint";
import {NativeAnimationCurveService} from "../native/NativeAnimationCurveService";

export type computedOutputs = {outputs: PerceptionWithOutputType[], perceptions: PerceptionType[]};

export class MaterialService {
    /**
     * find material by id in database
     * @param id
     */
    public static async getMaterialById(id: string): Promise<Material>{
        const material = await MaterialModel.findById(id);
        return material;
    }

    /***
     *
     * @param id
     * @param idUser
     * @private
     */
    private static getFindByIdAndUserFilters(id: string, idUser: string): {'_id':mongoose.Types.ObjectId, 'user':mongoose.Types.ObjectId}{
        const filter = {
            '_id': new mongoose.Types.ObjectId(id),
            'user': new mongoose.Types.ObjectId(idUser),
        };
        return filter;
    }

    /**
     *
     * @param id
     * @param idUser
     */
    public static async getMaterialByIdForUser(id: string, idUser: string): Promise<Material>{
        const filter = MaterialService.getFindByIdAndUserFilters(id, idUser);
        return MaterialModel.findOne(filter).select('+serialisation +perceptions +perceptionPoints');
    }

    /**
     *
     * @param id
     * @param idUser
     */
    public static async deleteMaterialById(id: string, idUser: string){
        const filter = MaterialService.getFindByIdAndUserFilters(id, idUser);
        return MaterialModel.deleteOne(filter);
    }

    /**
     *
     * @param id
     * @param idUser
     * @param serialisation
     * @param allocatedMaterialId
     * @param title
     */
    public static async updateMaterial(id: string, idUser: string,serialisation: string, allocatedMaterialId: number,title?: string){
        const material = await MaterialService.getMaterialByIdForUser(id, idUser);
        const deserializedMaterial: SerializedMaterial = JSON.parse(serialisation);
        title = title ?? material.title;
        const baseMaterial = MaterialService.createBaseMaterial(title, deserializedMaterial);
        material.title = baseMaterial.title;
        material.hd = baseMaterial.hd;
        material.volume = baseMaterial.volume;
        material.description = baseMaterial.description;
        material.serialisation = baseMaterial.serialisation;
        const output = MaterialService.computePerceptions(deserializedMaterial, allocatedMaterialId);
        material.perceptions = output.perceptions;
        material.perceptionPoints = output.outputs;
        return MaterialModel.updateOne({
            _id: new mongoose.Types.ObjectId(id),
        }, material);
    }



    /**
     * adds a new material
     * @param title
     * @param serializedMaterial
     * @param userId
     * @param allocatedMemoryId
     */
    public static async addMaterial(title: string,
                                    serializedMaterial: string,
                                    userId: string,
                                    allocatedMemoryId: number){
        const deserializedMaterial: SerializedMaterial = JSON.parse(serializedMaterial);
        const material = MaterialService.createBaseMaterial(title,deserializedMaterial);
        const output = MaterialService.computePerceptions(deserializedMaterial, allocatedMemoryId);
        material.perceptions = output.perceptions;
        material.perceptionPoints = output.outputs;
        const newMaterial: Material = await MaterialModel.create({
            ...material,
            user: userId,
        });
        return newMaterial;
    }

    /**
     *
     * @param allocatedMemoryId
     * @param perceptionId
     */
    public static computeKeyframes(allocatedMemoryId: number,perceptionId: PerceptionType): KeyframePoint[]{
        if(perceptionId === PerceptionType.STIFFNESS){
            const keyframes = NativeAnimationCurveService.getAllKeyFrames(
                allocatedMemoryId,
                perceptionId,
                0,
                0,
            );
            return keyframes.map(value => {
                const newKeyframe = new KeyframePoint();
                newKeyframe.coordinate = value.coordinate;
                newKeyframe.slope = value.tangent;
                return newKeyframe;
            });
        }
        return [];
    }

    /**
     *
     * @param deserialisedMaterial
     * @param allocatedMemoryId
     */
    public static computePerceptions(deserialisedMaterial: SerializedMaterial, allocatedMemoryId: number): computedOutputs{
        const material: computedOutputs = {
          outputs: [],
          perceptions: [],
        };
        if(deserialisedMaterial.m_texture){
            material.perceptions.push(PerceptionType.TEXTURE);
        }
        if(deserialisedMaterial.m_vibration){
            material.perceptions.push(PerceptionType.VIBRATION);
        }
        if(deserialisedMaterial.m_stiffness){
            material.perceptions.push(PerceptionType.STIFFNESS);
        }
        NativeMaterialService.loadMaterialAtIndex(allocatedMemoryId, JSON.stringify(deserialisedMaterial));
        if(material.perceptions.length > 0){
            material.perceptions.forEach((value => {
                const output = simplify(NativeEvaluateService.evaluatePerception(
                    allocatedMemoryId,
                    value,
                ), .01, true);
                material.outputs.push({
                    output,
                    perception: value,
                    keyframes: MaterialService.computeKeyframes(
                        allocatedMemoryId,
                        value,
                    ),
                });
            }));
        }
        return material;
    }

    /**
     *
     * @param title
     * @param deserializedMaterial
     */
    public static createBaseMaterial(title: string, deserializedMaterial: SerializedMaterial){
        const material: Material = new Material();
        material.title = title;
        material.hd = deserializedMaterial.m_HDFlag;
        material.volume = deserializedMaterial.m_volume;
        material.description = deserializedMaterial.m_description ?? '';
        material.serialisation = JSON.stringify(deserializedMaterial);
        return material;
    }

    /**
     *
     * @param userId
     * @param searchParams
     */
    public static getMaterialsForUser(userId: string, searchParams: UserMaterialSearch){
        const filter: any = {};
        filter['user'] = new mongoose.Types.ObjectId(userId);
        if(searchParams.keyword && searchParams.keyword !== ''){
            filter['$or'] = [
                {'title': { $regex: searchParams.keyword, $options: "i" }},
                {'description': { $regex: searchParams.keyword, $options: "i" }},
            ];
        }
        if(searchParams.perceptions && searchParams.perceptions.length > 0){
            filter['perceptions'] = {"$in": searchParams.perceptions}
        }
        const aggregateQuery = MaterialModel.aggregate([
            {$match: filter},
            {
                "$sort":{ "createdAt": -1 },
            }
        ]);
        return MaterialModel.aggregatePaginate(
            aggregateQuery,
            {
                page: searchParams.page,
                limit: 10,
            }
        );
    }
}
