import {KeyType} from "ioredis";
import {redis} from "../../config/redis";

export class RedisService {
    public static getValue(key: KeyType) {
        return redis.get(key);
    }
    public static setValue(key: KeyType, value: string, expireTime: number){
        return redis.setex(key,expireTime,value);
    }
    public static async addExpirationToValue(key: KeyType,expireTime: number){
        const value: string = await redis.get(key) as string;
        await redis.setex(key,expireTime, value);
    }
    public static expireKey(key: KeyType){
        return redis.del(key);
    }
}
