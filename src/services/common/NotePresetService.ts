import {Group} from "../../entity/models/Group";
import {PerceptionType} from "../../enum/PerceptionType";
import {KeyframePoint} from "../../entity/types/KeyframePoint";
import {AmplitudeModulation} from "../../entity/types/AmplitudeModulation";
import {CarrierSignal} from "../../entity/types/CarrierSignal";
import NotePresetModel, {NotePreset} from "../../entity/models/NotePreset";
import {NotePresetException} from "../../errors/NotePresetException";
import {NativePerceptionService} from "../native/NativePerceptionService";
import {NativeMelodyService} from "../native/NativeMelodyService";
import {NativeNoteService} from "../native/NativeNoteService";
import {NativeMaterialService} from "../native/NativeMaterialService";
import {AmplitudeModulationType} from "../../enum/AmplitudeModulationType";
import {NativeNoteDataService} from "../native/NativeNoteDataService";
import {NativeAnimationCurveService} from "../native/NativeAnimationCurveService";
import {NativeEvaluateService} from "../native/NativeEvaluateService";
import simplify from "simplify-js";
import {User} from "../../entity/models/User";
import {CoreExceptions} from "../../errors/CoreExceptions";
import {NotePresetSearch} from "../../utils/search/NotePresetSearch";
import {mongoose} from "@typegoose/typegoose";

export class NotePresetService {


    /**
     *
     * @param group
     * @param hd
     * @param title
     * @param description
     * @param isAmplitudeModulation
     * @param perceptionType
     * @param keyframes
     * @param originalLength
     * @param amplitudeData
     * @param carrierSignal
     * @param allocatedMaterialId
     */
    public static async createNotePreset(group: Group,
                                   hd: boolean,
                                   title: string,
                                   description: string,
                                   modulationType: AmplitudeModulationType,
                                   perceptionType: PerceptionType,
                                   keyframes: KeyframePoint[],
                                   originalLength: number,
                                   amplitudeData: AmplitudeModulation,
                                   carrierSignal: CarrierSignal,
                                   allocatedMaterialId: number): Promise<NotePreset>{
        if(hd && !carrierSignal){
            throw new Error(NotePresetException.CARRIER_SIGNAL_MISSING);
        }
        if(modulationType === AmplitudeModulationType.PERIODIC_SIGNAL && !amplitudeData){
            throw new Error(NotePresetException.AMPLITUDE_DATA_MISSING);
        }
        if(modulationType === AmplitudeModulationType.ANIMATION_CURVE && !keyframes){
            throw new Error(NotePresetException.KEYFRAME_LIST_MISSING);
        }
        const basicPlaces = NotePresetService.initPlaces(
            allocatedMaterialId,
            perceptionType,
        );
        NativeNoteService.setLength(
            allocatedMaterialId,
            perceptionType,
            basicPlaces.melodyId,
            basicPlaces.noteId,
            originalLength,
        );
        NativeMaterialService.setHd(allocatedMaterialId,hd);
        NativeNoteDataService.setModulationType(
            allocatedMaterialId,
            perceptionType,
            basicPlaces.melodyId,
            basicPlaces.noteId,
            modulationType,
        )
        const placementData = {
            materialId: allocatedMaterialId,
            perceptionId: perceptionType,
            melodyId: basicPlaces.melodyId,
            noteId: basicPlaces.noteId,
        };
        if(hd){
            NotePresetService.computeCarrierSignal(placementData,carrierSignal);
        }
        if(modulationType === AmplitudeModulationType.PERIODIC_SIGNAL){
            NotePresetService.computeAmplitudeModulation(
                placementData,
                amplitudeData,
            )
        }
        else{
            NotePresetService.computeKeyFrames(
                placementData,
                keyframes,
            );
        }
        NativeNoteService.setLength(
            allocatedMaterialId,
            perceptionType,
            basicPlaces.melodyId,
            basicPlaces.noteId,
            1,
        );
        const notePresetBaseData = NotePresetService.getFullNotePresetData(
            placementData
        );
        notePresetBaseData.title = title;
        notePresetBaseData.description = description;
        notePresetBaseData.createdAt = new Date();
        notePresetBaseData.group = group;
        const resultat: NotePreset = await NotePresetModel.create(notePresetBaseData);
        return resultat;
    }

    /**
     *
     * @param placementData
     * @param carrierSignal
     */
    public static computeCarrierSignal(placementData: {materialId: number, perceptionId: PerceptionType, melodyId: number,noteId: number},
                                       carrierSignal: CarrierSignal){
        NativeNoteService.applyCarrierSignal(
            placementData,
            carrierSignal,
        );
    }

    public static computeAmplitudeModulation(placementData: {materialId: number, perceptionId: PerceptionType, melodyId: number,noteId: number},
                                             amplitudeModulation: AmplitudeModulation){
        NativeNoteService.applyAmplitudeModulation(
            placementData,
            amplitudeModulation,
        );
    }
    public static computeKeyFrames(placementData: {materialId: number, perceptionId: PerceptionType, melodyId: number,noteId: number},
                                   keyframes: KeyframePoint[]){
        keyframes.forEach((keyframe, index) => {
            if(index === 0 || index === keyframes.length - 1){
                NativeAnimationCurveService.moveKeyFrame(
                    placementData.materialId,
                    placementData.perceptionId,
                    placementData.melodyId,
                    placementData.noteId,
                    index,
                    keyframe.coordinate.y,
                    keyframe.coordinate.x,
                    keyframe.slope,
                );
            }
            else{
                NativeAnimationCurveService.addKeyFrame(
                    placementData.materialId,
                    placementData.perceptionId,
                    placementData.melodyId,
                    placementData.noteId,
                    keyframe.coordinate.y,
                    keyframe.coordinate.x,
                    keyframe.slope,
                );
            }
        });
    }
    public static getFullNotePresetData(placementData: {materialId: number, perceptionId: PerceptionType, melodyId: number,noteId: number}): NotePreset{
        const notePreset = new NotePreset();
        notePreset.perception = placementData.perceptionId;
        notePreset.hd = NativeMaterialService.getHd(placementData.materialId);
        notePreset.amplitudeModulationType = NativeNoteDataService.getModulationType(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
        );
        notePreset.amplitudeCurve = simplify(NativeEvaluateService.evaluateAmplitudeModulation(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
        ),.01, true);
        notePreset.carrierSignal = NativeNoteDataService.getCarrierSignal(
            placementData.materialId,
            placementData.perceptionId,
            placementData.melodyId,
            placementData.noteId,
        );
        notePreset.carrierSignal.curve = simplify(notePreset.carrierSignal.curve, .01, true);
        if(notePreset.amplitudeModulationType === AmplitudeModulationType.PERIODIC_SIGNAL){
            notePreset.keyframes = [];
            notePreset.amplitudeModulation = NativeNoteDataService.getAmplitudeModulation(
                placementData.materialId,
                placementData.perceptionId,
                placementData.melodyId,
                placementData.noteId,
            );
        }
        else{
            notePreset.keyframes = NativeAnimationCurveService.getAllKeyFrames(
                placementData.materialId,
                placementData.perceptionId,
                placementData.melodyId,
                placementData.noteId,
            ).map(keyframe => ({coordinate:{x: keyframe.coordinate.x,y:keyframe.coordinate.y}, slope: keyframe.tangent}));
        }
        notePreset.resultCurve = simplify(
            NativeEvaluateService.evaluateResultNote(
                placementData.materialId,
                placementData.perceptionId,
                placementData.melodyId,
                placementData.noteId,
            ),.01, true,
        );
        return notePreset;
    }
    /**
     *
     * @param allocatedMaterialId
     * @param perceptionType
     */
    public static initPlaces(allocatedMaterialId: number,
                             perceptionType: PerceptionType,
                             ): {melodyId: number; noteId: number}{
        NativePerceptionService.addPerception(allocatedMaterialId,perceptionType);
        const melodyId = NativeMelodyService.addMelody(allocatedMaterialId,perceptionType);
        const noteId = NativeNoteService.addNote(allocatedMaterialId,perceptionType,melodyId, 0 );
        return {
            melodyId,
            noteId,
        }
    }

    public static async getNotePresetByIdForUser(id: string,idUser: string){
        const notePreset: NotePreset = await NotePresetModel.findById(id).populate('group').populate('group.user');
        const groupe: Group = notePreset.group as Group;
        const user: User = groupe.user;
        if(user._id.toString() !== idUser){
            throw new Error(CoreExceptions.ENTITY_NOT_FOUND);
        }
        return notePreset;
    }

    /**
     *
     * @param userId
     * @param searchParams
     */
    public static getNotePresetsForGroup(groupId: string,searchParams: NotePresetSearch){
        const filter: any =  {};
        filter['group'] = new mongoose.Types.ObjectId(groupId);
        if(searchParams.keyword && searchParams.keyword !== ''){
            filter['$or'] = [
                {'title': { $regex: searchParams.keyword, $options: "i" }},
                {'description': { $regex: searchParams.keyword, $options: "i" }},
            ];
        }
        filter['perception'] = searchParams.perceptionType;
        const aggregateQuery = NotePresetModel.aggregate([
            {$match: filter},
            {
                "$sort":{ "createdAt": -1 },
            }
        ]);
        return NotePresetModel.aggregatePaginate(
            aggregateQuery,
            {
                page: searchParams.page,
                limit: 10,
            }
        );
    }
}
