import argon2 from "argon2";
import UserModel, {UserWithId} from "../../entity/models/User";
import {JwtService} from "../../utils/services/JwtService";
import {AuthExceptions} from "../../errors/AuthExceptions";
import {RedisService} from "./RedisService";
import * as mongooseTypes from "mongoose";
export class AuthService {
    public static async registerUser(email: string, password: string): Promise<UserWithId>{
        const hashedPassword = await argon2.hash(password);
        let user: UserWithId = await UserModel.create({
            email: email,
            password: hashedPassword,
        });
        return user;
    }
    public static async login(email: string, password: string): Promise<UserWithId>{
        const user: UserWithId = await UserModel.findOne({ email });
        if(!user){
            throw new Error(AuthExceptions.USER_NOT_FOUND);
        }
        const valid = await argon2.verify(user.password, password);
        if(!valid){
            throw new Error(AuthExceptions.USER_NOT_FOUND);
        }
        return user;
    }
    public static async getProfile(userId: string): Promise<UserWithId>{
        const user: UserWithId = await UserModel.findById(userId);
        return user;
    }
    public static async createToken(userId: string): Promise<string>{
        const token = JwtService.createToken(userId);
        await RedisService.setValue(token,userId,parseInt(process.env.REDIS_EXPIRE as string));
        return token;
    }
    public static logout(token: string){
        return RedisService.expireKey(token);
    }
    public static async refreshToken(token: string){
        await RedisService.addExpirationToValue(token, parseInt(process.env.REDIS_EXPIRE as string));
    }
}
