import GroupModel, { Group } from "../../entity/models/Group";
import {mongoose} from "@typegoose/typegoose";
import {GroupSearch} from "../../utils/search/GroupSearch";
import {GroupTriggers} from "../../entity/triggers/GroupTriggers";

export class GroupService {
    public static async createGroup(name: string,
        description: string,
        userId: string,
        shared: boolean): Promise<Group> {
        const group = new Group();
        group.name = name;
        group.description = description;
        group.shared = shared;
        const newGroup = await GroupModel.create({
            ...group,
            user: userId
        });
        return newGroup;
    }
    /***
     *
     * @param id
     * @param idUser
     * @private
     */
    private static getFindByIdAndUserFilters(id: string, idUser: string): {'_id':mongoose.Types.ObjectId, 'user':mongoose.Types.ObjectId}{
        const filter = {
            '_id': new mongoose.Types.ObjectId(id),
            'user': new mongoose.Types.ObjectId(idUser),
        };
        return filter;
    }
    public static getGroupById(id: string,idUser: string): Promise<Group>{
        const filter = GroupService.getFindByIdAndUserFilters(id, idUser);
        return GroupModel.findOne(filter);
    }

    public static searchGroups(userId: string,searchParams: GroupSearch){
        const filter: any = {};
        filter['user'] = new mongoose.Types.ObjectId(userId);
        if(searchParams.keyword && searchParams.keyword !== ''){
            filter['$or'] = [
                {'name': { $regex: searchParams.keyword, $options: "i" }},
                {'description': { $regex: searchParams.keyword, $options: "i" }},
            ];
        }
        const aggregateQuery = GroupModel.aggregate([
            {$match: filter},
            {
                "$sort":{ "createdAt": -1 },
            }
        ]);
        return GroupModel.aggregatePaginate(
            aggregateQuery,
            {
                page: searchParams.page,
                limit: 10,
            }
        );
    }

    public static async deleteGroup(id: string){
        const filter: any = {
          '_id': new mongoose.Types.ObjectId(id),
        };
        const group = await GroupModel.deleteOne(filter);
        await GroupTriggers.deleteOneGroupTrigger(id);
    }
    public static updateGroup(id: string, data: Pick<Group, 'description' | 'name' | 'shared'>){
        const filter: any = {
            '_id': new mongoose.Types.ObjectId(id),
        };
        return GroupModel.updateOne(filter, { $set:{
            name: data.name,
            description: data.description,
            shared: data.shared,
        }});
    }
}
