import {Point} from "./Point";

export interface KeyFrame {
    id: number;
    materialId: number;
    perceptionId?: number;
    melodyId?: number;
    noteId?: number;
    coordinate: Point;
    tangent: number;
    slopeAngle?: number;
    tangentCurve?: Point[];
}
