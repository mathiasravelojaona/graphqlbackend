import {Field, InputType, ObjectType} from "type-graphql";
import {PatternFunctions} from "../../enum/PatternFunctions";
import {mongoose, prop} from "@typegoose/typegoose";
import {Point} from "./Point";

@ObjectType()
@InputType('CarrierSignalInput')
export class CarrierSignal {

    @prop({ enum: PatternFunctions, type: Number})
    @Field(() => Number,{nullable: false})
    pattern: PatternFunctions;

    @prop({type: Number,default: 0})
    @Field(() => Number, {nullable: false})
    frequency: number;

    @Field(() => [Point],{nullable: true})
    @prop({type: mongoose.Schema.Types.Array,  required: false,_id: false, select: false})
    curve: Point[];
}
