import {Field, ObjectType} from "type-graphql";
import {PerceptionType} from "../../enum/PerceptionType";
import {mongoose, prop} from "@typegoose/typegoose";
import {Point} from "./Point";
import {KeyframePoint} from "./KeyframePoint";

@ObjectType()
export class PerceptionWithOutputType {

    @prop({ enum: PerceptionType, type: Number})
    @Field(() => PerceptionType,{nullable: false})
    perception: PerceptionType;

    @prop({type: mongoose.Schema.Types.Array, default: [], _id: false})
    @Field(() => [Point], {nullable: false})
    output: Point[];

    @prop({type: mongoose.Schema.Types.Array, default: [], _id: false})
    @Field(() => [KeyframePoint], {nullable: false})
    keyframes: KeyframePoint[];
}
