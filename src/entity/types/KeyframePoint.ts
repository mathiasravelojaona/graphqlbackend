import {Point} from "./Point";
import {Field, InputType, ObjectType} from "type-graphql";
import {prop} from "@typegoose/typegoose";

@ObjectType()
@InputType('KeyframePointInput')
export class KeyframePoint {

    @prop({type: Point, default: [], _id: false})
    @Field(() => Point, {nullable: false})
    coordinate: Point;

    @prop({type: Number,default: 0})
    @Field(() => Number, {nullable: false})
    slope: number;
}
