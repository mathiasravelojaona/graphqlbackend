import {Field, InputType, ObjectType} from "type-graphql";
import {PatternFunctions} from "../../enum/PatternFunctions";
import {prop} from "@typegoose/typegoose";

@ObjectType()
@InputType('AmplitudeModulationInput')
export class AmplitudeModulation {
    @prop({type: Number,default: 0})
    @Field(() => Number, {nullable: false})
    phase: number;

    @prop({type: Number,default: 0})
    @Field(() => Number, {nullable: false})
    offset: number;

    @prop({ enum: PatternFunctions, type: Number})
    @Field(() => Number,{nullable: false})
    pattern: PatternFunctions;

    @prop({type: Number,default: 0})
    @Field(() => Number, {nullable: false})
    frequency: number;

    @prop({type: Number,default: 0})
    @Field(() => Number, {nullable: false})
    amplitude: number;

}
