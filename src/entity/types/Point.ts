import {prop} from "@typegoose/typegoose";
import {Field, InputType, ObjectType} from "type-graphql";


@ObjectType()
@InputType('pointInput')
export class Point {
    @Field()
    @prop({type: () => Number,required: true})
    x: number;

    @Field()
    @prop({type: () => Number,required: true})
    y: number;
}
