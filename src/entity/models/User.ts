import { Field, ObjectType } from "type-graphql";
import {getModelForClass, prop} from "@typegoose/typegoose";
import * as mongooseTypes from "mongoose";
export type UserWithId = User & {_id: mongooseTypes.ObjectId};

@ObjectType()
export class User{

    @Field({nullable: false})
    _id: string;

    @Field({nullable: false})
    @prop({unique: true, required: true})
    email!: string;

    @prop({required: true})
    password!: string;
}
const UserModel = getModelForClass(User);
export default UserModel;
