import { Field, ObjectType } from "type-graphql";
import { User, UserWithId } from "./User";
import {prop, Ref, plugin, buildSchema, addModelToTypegoose, post, getModelForClass} from "@typegoose/typegoose";
import mongoosePaginate from 'mongoose-aggregate-paginate-v2'
import {TimeStamps} from "@typegoose/typegoose/lib/defaultClasses";
import {GroupTriggers} from "../triggers/GroupTriggers";
import {mongoose} from "@typegoose/typegoose";
@ObjectType()
@plugin(mongoosePaginate)
export class Group extends TimeStamps{
    @Field()
    _id!: string;

    @Field()
    @prop()
    shared!: boolean;

    @Field()
    @prop()
    name!: string;

    @Field()
    @prop({ select: false })
    description!: string;

    @prop({ ref: () => User, required: true })
    user!: Ref<UserWithId>

}

// @ts-ignore
const GroupModel = getModelForClass(Group);
export default GroupModel;
