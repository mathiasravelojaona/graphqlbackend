import {getModelForClass, mongoose, plugin, prop, Ref} from "@typegoose/typegoose";
import {User, UserWithId} from "./User";
import {Field, ObjectType} from "type-graphql";
import {PerceptionType} from "../../enum/PerceptionType";
import {PerceptionWithOutputType} from "../types/PerceptionWithOutputType";
import mongoosePaginate from "mongoose-aggregate-paginate-v2";
import {TimeStamps} from "@typegoose/typegoose/lib/defaultClasses";

@ObjectType()
@plugin(mongoosePaginate)
export class Material extends TimeStamps{
    @Field({nullable: false})
    _id: string;

    @Field()
    @prop()
    updatedAt?: Date;

    @Field()
    @prop()
    createdAt?: Date;

    @Field({nullable: false})
    @prop({ required: true })
    title!: string;

    @Field({nullable: false})
    @prop({default: ''})
    description!: string;

    @Field({nullable: false})
    @prop({ required: true })
    hd!: boolean;

    @Field({nullable: false})
    @prop({ required: true })
    volume!: number;

    @prop({ required: true, select: false })
    serialisation!: string;

    @prop({ref: () => User, required: true, select: false})
    user!: Ref<UserWithId>;

    @prop({ enum: PerceptionType, type: Number})
    @Field(() => [Number],{nullable: false})
    perceptions!: PerceptionType[];

    @prop({type: () => mongoose.Schema.Types.Array, _id: false, select: false})
    perceptionPoints!: PerceptionWithOutputType[];
}

let MaterialModel = getModelForClass(Material);
export default MaterialModel;
