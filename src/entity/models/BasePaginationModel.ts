import {ClassType, Field, ObjectType} from "type-graphql";

export default function BasePaginationModel<TItem>(TItemClass: ClassType<TItem>){
    @ObjectType({ isAbstract: true })
    abstract class BasePaginationModelClass{
        @Field(type => [TItemClass])
        docs: TItem[];

        @Field()
        totalDocs: number;

        @Field()
        limit: number;

        @Field()
        page: number;

        @Field()
        totalPages: number;

        @Field()
        hasPrevPage: boolean;

        @Field()
        hasNextPage: boolean;
    }
    return BasePaginationModelClass;
}

