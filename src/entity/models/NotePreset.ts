import {Field, ObjectType} from "type-graphql";
import {getModelForClass, mongoose, plugin, prop, Ref} from "@typegoose/typegoose";
import {PerceptionType} from "../../enum/PerceptionType";
import {Group} from "./Group";
import {Point} from "../types/Point";
import {KeyframePoint} from "../types/KeyframePoint";
import {CarrierSignal} from "../types/CarrierSignal";
import {AmplitudeModulation} from "../types/AmplitudeModulation";
import {AmplitudeModulationType} from "../../enum/AmplitudeModulationType";
import mongoosePaginate from "mongoose-aggregate-paginate-v2";

@plugin(mongoosePaginate)
@ObjectType()
export class NotePreset {
    @Field({nullable: false})
    _id: string;

    @Field()
    @prop()
    updatedAt?: Date;

    @Field()
    @prop()
    createdAt?: Date;

    @Field({nullable: false})
    @prop({ required: true })
    title!: string;

    @Field({nullable: false})
    @prop({default: ''})
    description!: string;

    @Field({nullable: false})
    @prop({ required: true })
    hd!: boolean;

    @prop({ enum: PerceptionType, type: Number})
    @Field(() => Number,{nullable: false})
    perception!: PerceptionType;

    @Field(() => Group, { nullable: false })
    @prop({ ref: Group, required: true })
    group!: Ref<Group>;

    @prop({ enum: AmplitudeModulationType, type: Number})
    @Field(() => Number,{nullable: false})
    amplitudeModulationType!: AmplitudeModulationType;

    @prop({type: mongoose.Schema.Types.Array, _id: false, select: false})
    @Field(() => [Point], {nullable: false})
    resultCurve: Point[]

    @prop({type: mongoose.Schema.Types.Array, _id: false, select: false})
    @Field(() => [Point], {nullable: false})
    amplitudeCurve: Point[];

    @prop({type: mongoose.Schema.Types.Array, default: [], _id: false, select: false})
    @Field(() => [KeyframePoint], {nullable: false})
    keyframes: KeyframePoint[];

    @Field(() => CarrierSignal,{nullable: false})
    @prop({type: CarrierSignal,  required: true, _id: false, select: false})
    carrierSignal: CarrierSignal;

    @Field(() => AmplitudeModulation,{nullable: true})
    @prop({type: AmplitudeModulation,  required: false, _id: false, select: false})
    amplitudeModulation: AmplitudeModulation;
}
let NotePresetModel = getModelForClass(NotePreset);
export default NotePresetModel;
