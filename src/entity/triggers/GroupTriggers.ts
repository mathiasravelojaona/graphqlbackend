import {mongoose} from "@typegoose/typegoose";
import {Group} from "../models/Group";
import NotePresetModel from "../models/NotePreset";

export class GroupTriggers {
    public static deleteOneGroupTrigger(id: string){
        const filter: any = {};
        filter['group'] = new mongoose.Types.ObjectId(id);
        return  NotePresetModel.deleteMany(filter);
    }
}
